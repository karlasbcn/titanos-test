## Boilerplate

Used `redux-typescript` to bootstrap create-react-app

`npx create-react-app titanos-test --template redux-typescript`

## How to run it

Use `npm install` to install dependencies. Then use `npm start` to open app locally in dev mode. Run `npm test` to run tests

## Strategy

When app starts, first page is fetched from API. Once items are loaded, first item is set as active.

Not all items are rendered to avoid filling the app with too many divs: we just want to render the active item, some more items after it (enough to fill the wiewport), and one item before it.

When arrow key is pressed, we set the previous/next item as active. When this happens:

1. New active item is highlighted
2. Transition is enabled and we translate the list horizontally about one item width more or less
3. When transition is completed, we re-calculate the items to be rendered, based on the new active item. This can include the very first items if active item is almost at the queue of the list

When an active item is near the queue, more items are fetched (unless we already fetched all)

## Missing improvements

1. Write more tests. Find out why test involving arrow key pressing is not working (check last test at `App.spec.tsx`)
2. Responsiveness: Size of items could be proportional to viewport size
3. Fix weird transition when moving from the last item from the collection to first one having all the collection fetched
