export type Response = {
  readonly collection: CollectionItem[]
  readonly pagination: Pagination
}

export type CollectionItem = {
  readonly id: number
  readonly type: string
  readonly original_title: string
  readonly title: string
  readonly synopsis: string
  readonly year: number
  readonly classification: Classification
  readonly duration_in_seconds: number | null
  readonly number: number | null
  readonly parent_id: number | null
  readonly images: Images
  readonly stream_info: {}
  readonly genres: Genre[]
  readonly deeplinkings: Deeplinking[]
}

export type Classification = {
  readonly id: number
  readonly name: string
  readonly description: string
}

export type Deeplinking = {
  readonly app: App
  readonly url: string
}

export type App = {
  readonly id: number
  readonly name: string
  readonly code: string
  readonly icon: string
  readonly type: string
  readonly classification: Classification
  readonly image_dominant_color: string
  readonly url: string
  readonly app_type: string
  readonly resolution: string
  readonly content_list_app_type: string
  readonly content_list_app_name: string
  readonly description: string
  readonly background_image: string
  readonly logo: string | null
  readonly icon_landscape: string
  readonly background_image_dominant_color: string | null
  readonly icon_landscape_dominant_color: string | null
  readonly icon_dominant_color: string
}

export type Genre = {
  readonly id: number
  readonly name: string
  readonly image: Image
}

export type Image = {
  readonly id: number
  readonly locale: string
  readonly image: string
  readonly image_dominant_color: string
  readonly image_image_dominant_color: string
}

export type Images = {
  readonly id: number
  readonly locale: string
  readonly artwork_portrait: string
  readonly artwork_landscape: string | null
  readonly screenshot_portrait: null
  readonly screenshot_landscape: string
  readonly image_dominant_color: string
  readonly transparent_logo: null
  readonly artwork_portrait_dominant_color: string | null
  readonly artwork_landscape_dominant_color: string | null
  readonly screenshot_portrait_dominant_color: string | null
  readonly screenshot_landscape_dominant_color: string | null
}

export type Pagination = {
  readonly count: number
  readonly per_page: number
  readonly current_page: number
  readonly next_page: number | null
  readonly prev_page: number | null
  readonly total_count: number
  readonly total_pages: number
}
