import { useEffect, useState } from 'react'
import clsx from 'clsx'
import type { CollectionItem } from '../types'
import styles from './Item.module.css'
import { prefetchItemImage } from '../helpers'
import { ITEM_WIDTH } from '../constants'

interface ItemProps {
  item: CollectionItem
  isActive: boolean
}

export function Item({ item, isActive }: ItemProps) {
  const [img, setImg] = useState<string>()

  useEffect(() => {
    prefetchItemImage(item).then(setImg)
  }, [item])

  return (
    <div
      className={styles.container}
      style={{
        width: ITEM_WIDTH,
        backgroundColor:
          item.images.artwork_landscape_dominant_color ?? undefined,
      }}
    >
      {img ? (
        <img
          className={clsx(styles.image, isActive && styles.active)}
          src={img}
          alt={item.title}
          width={ITEM_WIDTH}
        />
      ) : null}
      <div className={styles.title} style={{ width: ITEM_WIDTH }}>
        {item.title}
      </div>
    </div>
  )
}
