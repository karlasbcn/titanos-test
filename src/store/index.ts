import { configureStore } from '@reduxjs/toolkit'
import { reducer, slice, type AppState } from './reducer'

export const store = configureStore({ reducer })
export { fetchCollectionItems } from './reducer'
export const { shiftActiveItem } = slice.actions
export type { AppState }
