import {
  type PayloadAction,
  createAsyncThunk,
  createSlice,
} from '@reduxjs/toolkit'
import type { CollectionItem, Pagination, Response } from '../types'
import { fetchAPI } from '../helpers'

export interface AppState {
  collection: CollectionItem[]
  pagination: Pagination | null
  activeItem: CollectionItem['id'] | null
}

export const initialState: AppState = {
  collection: [],
  pagination: null,
  activeItem: null,
}

export const fetchCollectionItems = createAsyncThunk(
  'state/fetchItems',
  async (_, { getState }) => {
    const page = (getState() as AppState).pagination?.next_page
    if (page === null) {
      return false
    }
    const response = await fetchAPI(page ?? 1)
    return response
  }
)

export const slice = createSlice({
  name: 'state',
  initialState,
  reducers: {
    shiftActiveItem(state, action: PayloadAction<number>) {
      const offset = action.payload
      const collectionLength = state.collection.length
      const allFetched = state.pagination?.next_page === null
      const currentIndex = state.collection.findIndex(
        item => item.id === state.activeItem
      )
      let nextIndex = currentIndex + offset
      if (nextIndex < 0) {
        if (!allFetched) {
          return
        }
        nextIndex += collectionLength
      }
      if (nextIndex >= collectionLength) {
        if (!allFetched) {
          return
        }
        nextIndex -= collectionLength
      }
      state.activeItem = state.collection[nextIndex]?.id ?? null
    },
  },
  extraReducers: builder => {
    builder.addCase(fetchCollectionItems.fulfilled, (state, action) => {
      if (!action.payload) return
      const { collection, pagination } = action.payload as Response
      if (pagination.current_page <= (state.pagination?.current_page ?? 0)) {
        return
      }
      state.collection = [...state.collection, ...collection]
      state.pagination = pagination
      if (state.activeItem === null) {
        state.activeItem = collection[0].id
      }
    })
  },
})

export const { reducer } = slice
