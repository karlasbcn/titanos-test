import { nextPagination, stub } from '../test/stubs'
import {
  reducer,
  initialState,
  type AppState,
  fetchCollectionItems,
} from './reducer'
import { fetchAPI } from '../helpers'
import { store } from '.'

jest.mock('../helpers')

const mockedFetchAPI = fetchAPI as jest.Mock<ReturnType<typeof fetchAPI>>

describe('reducer', () => {
  it('should handle initial state', () => {
    const output = reducer(initialState, { type: 'unknown' })
    expect(output).toEqual(initialState)
  })

  it('should handle shiftActiveItem', () => {
    const state: AppState = {
      ...stub,
      activeItem: stub.collection[0].id,
    }
    const action = {
      type: 'state/shiftActiveItem',
      payload: 1,
    }
    const outputNext = reducer(state, action)
    expect(outputNext.activeItem).toEqual(stub.collection[1].id)
    const outputPrev = reducer(outputNext, { ...action, payload: -1 })
    expect(outputPrev.activeItem).toEqual(stub.collection[0].id)
    const outputEmpty = reducer(initialState, action)
    expect(outputEmpty.activeItem).toEqual(initialState.activeItem)
  })

  it('should handle fetchCollectionItems', async () => {
    mockedFetchAPI.mockResolvedValueOnce(stub)
    const action = await store.dispatch(fetchCollectionItems())
    const output = store.getState()
    expect(action.payload).toEqual(stub)
    expect(output.collection.length).toEqual(stub.collection.length)
    expect(output.activeItem).toEqual(stub.collection[0].id)
    expect(output.pagination?.current_page).toEqual(1)
    expect(mockedFetchAPI).toHaveBeenCalledWith(1)
    expect(mockedFetchAPI).toHaveBeenCalledTimes(1)

    mockedFetchAPI.mockResolvedValueOnce({
      ...stub,
      pagination: nextPagination,
    })
    await store.dispatch(fetchCollectionItems())
    const output2 = store.getState()
    expect(output2.collection.length).toEqual(stub.collection.length * 2)
    expect(output2.pagination?.current_page).toEqual(2)
    expect(mockedFetchAPI).toHaveBeenCalledWith(output.pagination?.next_page)
    expect(mockedFetchAPI).toHaveBeenCalledTimes(2)
  })
})
