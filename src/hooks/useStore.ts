import {
  type TypedUseSelectorHook,
  useSelector,
  useDispatch,
} from 'react-redux'
import { store, type AppState } from '../store'

const useAppSelector: TypedUseSelectorHook<AppState> = useSelector

export const useAppDispatch = () => useDispatch<typeof store.dispatch>()

export const useStoreData = () => {
  const collection = useAppSelector(state => state.collection)
  const activeItem = useAppSelector(state => state.activeItem)
  const activeIndex = collection.findIndex(item => item.id === activeItem)
  return { collection, activeItem, activeIndex }
}
