import { useEffect } from 'react'
import { useAppDispatch } from './useStore'
import { shiftActiveItem } from '../store'

export const useKeyboard = (enable: boolean): void => {
  const dispatch = useAppDispatch()

  useEffect(() => {
    const handler = ({ key }: KeyboardEvent) => {
      if (key === 'ArrowLeft') {
        dispatch(shiftActiveItem(-1))
      }
      if (key === 'ArrowRight') {
        dispatch(shiftActiveItem(1))
      }
    }

    if (enable) {
      document.addEventListener('keydown', handler)
    }

    return () => {
      document.removeEventListener('keydown', handler)
    }
  }, [enable, dispatch])
}
