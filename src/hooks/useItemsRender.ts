import { useEffect, useRef, useState } from 'react'
import { ITEM_GAP, ITEM_WIDTH } from '../constants'
import type { CollectionItem } from '../types'
import { useStoreData } from './useStore'

export function useItemsRender() {
  const { collection, activeIndex } = useStoreData()
  const [isTransitioning, setIsTransitioning] = useState(false)
  const [horizontalOffset, setHorizontalOffset] = useState(0)
  const [items, setItems] = useState<CollectionItem[]>([])
  const activeIndexRef = useRef(-1)
  const prevIndexRef = useRef(-1)

  useEffect(() => {
    if (activeIndexRef.current > -1) {
      setIsTransitioning(true)
    }
    prevIndexRef.current = activeIndexRef.current
    activeIndexRef.current = activeIndex
  }, [activeIndex])

  useEffect(() => {
    const activeIndex = activeIndexRef.current
    if (isTransitioning) {
      setHorizontalOffset(prevHorizontalOffset => {
        const shift = prevIndexRef.current < activeIndex ? -1 : 1
        return Math.max(
          -2 * ITEM_WIDTH - ITEM_GAP,
          prevHorizontalOffset + shift * (ITEM_WIDTH + ITEM_GAP)
        )
      })
    } else {
      const sliceFrom = Math.max(0, activeIndex - 1)
      const numItemsOnScreen =
        Math.ceil(window.innerWidth / (ITEM_WIDTH + ITEM_GAP)) + 2
      let items = collection.slice(sliceFrom, sliceFrom + numItemsOnScreen)
      if (items.length < numItemsOnScreen) {
        items = [
          ...items,
          ...collection.slice(0, numItemsOnScreen - items.length),
        ]
      }
      setItems(items)
      setHorizontalOffset(activeIndex > 0 ? -ITEM_WIDTH : ITEM_GAP)
    }
  }, [isTransitioning, collection])

  return {
    items,
    isTransitioning,
    horizontalOffset,
    onTransitionEnd: isTransitioning
      ? () => setIsTransitioning(false)
      : undefined,
  }
}
