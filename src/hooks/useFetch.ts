import { useEffect } from 'react'
import { FETCH_MORE_THRESHOLD } from '../constants'
import { fetchCollectionItems } from '../store'
import { useAppDispatch, useStoreData } from './useStore'

export const useFetch = (): void => {
  const { collection, activeIndex } = useStoreData()
  const dispatch = useAppDispatch()
  const collectionLength = collection.length

  useEffect(() => {
    if (collectionLength - activeIndex < FETCH_MORE_THRESHOLD) {
      dispatch(fetchCollectionItems())
    }
  }, [activeIndex, collectionLength, dispatch])
}
