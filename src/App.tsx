import clsx from 'clsx'
import styles from './App.module.css'
import { Item } from './components'
import { ITEM_GAP } from './constants'
import { useStoreData, useItemsRender, useKeyboard, useFetch } from './hooks'

export function App() {
  const { activeItem } = useStoreData()
  const { items, horizontalOffset, isTransitioning, onTransitionEnd } =
    useItemsRender()
  useKeyboard(!isTransitioning)
  useFetch()

  if (items.length === 0) {
    return <div className={styles.loading}>Loading...</div>
  }

  return (
    <div
      className={clsx(styles.container, isTransitioning && styles.transition)}
      style={{ gap: ITEM_GAP, transform: `translateX(${horizontalOffset}px)` }}
      onTransitionEnd={onTransitionEnd}
    >
      {items.map(item => (
        <Item key={item.id} item={item} isActive={item.id === activeItem} />
      ))}
    </div>
  )
}
