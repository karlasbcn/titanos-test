import type { CollectionItem } from '../types'

export const prefetchItemImage = (item: CollectionItem) =>
  new Promise<string>((resolve, reject) => {
    const img = new Image()
    const src = item.images.artwork_portrait
    img.onload = () => resolve(src)
    img.onerror = () => reject()
    img.src = src
  })
