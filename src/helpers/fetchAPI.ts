import { BASE_URL } from '../constants'
import type { Response } from '../types'

export const fetchAPI: (
  page: number,
  pageSize?: number
) => Promise<Response | null> = async (page, pageSize = 20) => {
  const url = `${BASE_URL}/genres/14/contents?market=es&device=tv&locale=es&page=${page}&per_page=${pageSize}`
  try {
    const response = await fetch(url)
    const data: Response = await response.json()
    return data
  } catch (error) {
    console.error('Error fetching data:', error)
    return null
  }
}
