export const ITEM_GAP = 24
export const ITEM_WIDTH = 260

export const BASE_URL = 'https://acc01.titanos.tv/v1'

export const FETCH_MORE_THRESHOLD = 10
