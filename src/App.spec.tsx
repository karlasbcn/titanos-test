import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import { App } from './App'
import { Provider } from 'react-redux'
import { store } from './store'

import { fetchAPI, prefetchItemImage } from './helpers'
import { stub } from './test/stubs'

jest.setTimeout(10000)
jest.mock('./helpers')

export const mockedFetchAPI = fetchAPI as jest.Mock<ReturnType<typeof fetchAPI>>
export const mockedPrefetchItemImage = prefetchItemImage as jest.Mock<
  ReturnType<typeof prefetchItemImage>
>

describe('app', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    mockedFetchAPI.mockResolvedValue(stub)
    mockedPrefetchItemImage.mockResolvedValue('image')
    window.innerWidth = 1024
  })
  it('should work', async () => {
    const { collection } = stub

    render(
      <Provider store={store}>
        <App />
      </Provider>
    )
    expect(screen.getByText('Loading...')).toBeInTheDocument()

    await screen.findByText(collection[0].title)

    expect(screen.queryByText('Loading...')).not.toBeInTheDocument()

    await screen.findAllByRole('img')

    const numVisibleImages = screen.getAllByRole('img').length
    let i = 0
    while (i < numVisibleImages) {
      expect(screen.getByText(collection[i].title)).toBeInTheDocument()
      i++
    }

    expect(screen.queryByText(collection[i].title)).not.toBeInTheDocument()

    //TODO: next item should be in the document (don't know why it's not)

    /*
      fireEvent.keyDown(document, { key: 'ArrowRight' })
      await new Promise(r => setTimeout(r, 1000))
      fireEvent.keyDown(document, { key: 'ArrowRight' })
      await new Promise(r => setTimeout(r, 1000))
      await screen.findByText(collection[i].title)
      expect(screen.getByText(collection[i].title)).toBeInTheDocument()
    */
  })
})
