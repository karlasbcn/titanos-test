import type { Pagination, Response } from '../types'

export const stub: Response = {
  collection: [
    {
      id: 30,
      type: 'movie',
      original_title: 'El Ambulante',
      title: 'El Ambulante',
      synopsis:
        '¿Puede hacer un hombre solo hacer soñar a todo un pueblo? Un hombre llega con un coche destartalado a un pequeño pueblo de la Pampa argentina. Se reúne con las autoridades del municipio y les hace una propuesta: realizar un largometraje de ficción en 30 días a cambio de comida y alojamiento. Los actores: los vecinos. Ésta es la vida de Daniel Burmeister, que recorre Argentina de pueblo en pueblo creando historias únicas que inmortaliza en películas. En cada parada adapta sus guiones y consigue ingresos adicionales con la venta de las entradas y los DVDs. ',
      year: 2010,
      classification: {
        id: 9,
        name: 'APTA',
        description: 'Universal: suitable for all',
      },
      duration_in_seconds: null,
      number: null,
      parent_id: null,
      images: {
        id: 38919,
        locale: 'es',
        artwork_portrait:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/75l8icc6wjj8d6462hlrdtr2odh5',
        artwork_landscape: null,
        screenshot_portrait: null,
        screenshot_landscape:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/gcxtflf76muj2an02l2ntpalem6v',
        image_dominant_color: '#000000',
        transparent_logo: null,
        artwork_portrait_dominant_color: null,
        artwork_landscape_dominant_color: '#000000',
        screenshot_portrait_dominant_color: null,
        screenshot_landscape_dominant_color: '#FF5100',
      },
      stream_info: {},
      genres: [
        {
          id: 10,
          name: 'Documentales y especiales',
          image: {
            id: 10,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/9t4iec0n10oxh7c7y4aqcaw811wx',
            image_dominant_color: '#FF5E00',
            image_image_dominant_color: '#FF5E00',
          },
        },
        {
          id: 14,
          name: 'Indie',
          image: {
            id: 14,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/nbv025yrsssr42n26oxxpkv1qlgu',
            image_dominant_color: '#00A1FF',
            image_image_dominant_color: '#00A1FF',
          },
        },
      ],
      deeplinkings: [
        {
          app: {
            id: 197,
            name: 'Rakuten TV',
            code: 'rakuten_tv',
            icon: 'https://d998oz1ebgwp7.cloudfront.net:443/img/m7l2xyokayosn3d4j2e93ts5a3fw',
            type: 'app',
            classification: {
              id: 9,
              name: 'APTA',
              description: 'Universal: suitable for all',
            },
            image_dominant_color: '#000000',
            url: 'https://prod-philips-ui40-app.rakuten.tv/',
            app_type: 'NetTV',
            resolution: '1080p',
            content_list_app_type: 'rakuten_tv_list',
            content_list_app_name: 'Últimas películas de Rakuten TV',
            description:
              'Rakuten TV es una de las principales plataformas de vídeo bajo demanda en Europa',
            background_image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/4e82tsmi4azt096ofg6r03crnzjd',
            logo: null,
            icon_landscape:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/8jd28avlz42odrxmfm5f0xvxm2e4',
            background_image_dominant_color: null,
            icon_landscape_dominant_color: null,
            icon_dominant_color: '#000000',
          },
          url: 'https://prod-philips-ui40-app.rakuten.tv/?titleid=$movies/el-ambulante-2010',
        },
      ],
    },
    {
      id: 96,
      type: 'movie',
      original_title: 'Band Aid',
      title: 'Band Aid',
      synopsis:
        'Una delicia exuberantemente discreta que utiliza un toque ligero e irónico para abordar temas tan importantes como el impulso artístico, la inercia y la división entre hombres y mujeres. Una pareja que no puede dejar de pelearse se embarca en un último esfuerzo para salvar su matrimonio: convertir sus peleas en canciones y comenzar una banda.',
      year: 2017,
      classification: {
        id: 13,
        name: '16',
        description: 'Not recommended for children under 16 years',
      },
      duration_in_seconds: null,
      number: null,
      parent_id: null,
      images: {
        id: 72499,
        locale: 'es',
        artwork_portrait:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/hsi46mj7gb5v4mc33dhfm6anh1o0',
        artwork_landscape: null,
        screenshot_portrait: null,
        screenshot_landscape:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/ey0dokwrlp29riitdiu31h5tumlm',
        image_dominant_color: '#000000',
        transparent_logo: null,
        artwork_portrait_dominant_color: null,
        artwork_landscape_dominant_color: null,
        screenshot_portrait_dominant_color: null,
        screenshot_landscape_dominant_color: '#FF7B00',
      },
      stream_info: {},
      genres: [
        {
          id: 8,
          name: 'Comedia',
          image: {
            id: 8,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/13ypbzbnsgayd85gnuo72g5el7l0',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 11,
          name: 'Drama',
          image: {
            id: 11,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/htkmlixr0gd1ogf5t4aid2mazhey',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 14,
          name: 'Indie',
          image: {
            id: 14,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/nbv025yrsssr42n26oxxpkv1qlgu',
            image_dominant_color: '#00A1FF',
            image_image_dominant_color: '#00A1FF',
          },
        },
      ],
      deeplinkings: [
        {
          app: {
            id: 197,
            name: 'Rakuten TV',
            code: 'rakuten_tv',
            icon: 'https://d998oz1ebgwp7.cloudfront.net:443/img/m7l2xyokayosn3d4j2e93ts5a3fw',
            type: 'app',
            classification: {
              id: 9,
              name: 'APTA',
              description: 'Universal: suitable for all',
            },
            image_dominant_color: '#000000',
            url: 'https://prod-philips-ui40-app.rakuten.tv/',
            app_type: 'NetTV',
            resolution: '1080p',
            content_list_app_type: 'rakuten_tv_list',
            content_list_app_name: 'Últimas películas de Rakuten TV',
            description:
              'Rakuten TV es una de las principales plataformas de vídeo bajo demanda en Europa',
            background_image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/4e82tsmi4azt096ofg6r03crnzjd',
            logo: null,
            icon_landscape:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/8jd28avlz42odrxmfm5f0xvxm2e4',
            background_image_dominant_color: null,
            icon_landscape_dominant_color: null,
            icon_dominant_color: '#000000',
          },
          url: 'https://prod-philips-ui40-app.rakuten.tv/?titleid=$movies/band-aid',
        },
      ],
    },
    {
      id: 120,
      type: 'movie',
      original_title: 'The lovers',
      title: 'The Lovers',
      synopsis:
        'Mary y Michael forman un matrimonio de mediana edad que tras años de rutina ambos tienen un amante, y parecen aceptarlo en silencio. Pero un día todo cambia y se vuelven a enamorar.',
      year: 2017,
      classification: {
        id: 14,
        name: '18',
        description: 'Not recommended for children under 18 years',
      },
      duration_in_seconds: null,
      number: null,
      parent_id: null,
      images: {
        id: 72271,
        locale: 'es',
        artwork_portrait:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/hlnignbch97924kdt4xbaxtz48w7',
        artwork_landscape: null,
        screenshot_portrait: null,
        screenshot_landscape:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/palxrx0q3bj4huemhhv7y79ej5mk',
        image_dominant_color: '#000000',
        transparent_logo: null,
        artwork_portrait_dominant_color: '#FF6A00',
        artwork_landscape_dominant_color: null,
        screenshot_portrait_dominant_color: null,
        screenshot_landscape_dominant_color: null,
      },
      stream_info: {},
      genres: [
        {
          id: 8,
          name: 'Comedia',
          image: {
            id: 8,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/13ypbzbnsgayd85gnuo72g5el7l0',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 11,
          name: 'Drama',
          image: {
            id: 11,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/htkmlixr0gd1ogf5t4aid2mazhey',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 14,
          name: 'Indie',
          image: {
            id: 14,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/nbv025yrsssr42n26oxxpkv1qlgu',
            image_dominant_color: '#00A1FF',
            image_image_dominant_color: '#00A1FF',
          },
        },
        {
          id: 19,
          name: 'Romance',
          image: {
            id: 19,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/5f6eiyrpd8cw29lh0ei9txmws5or',
            image_dominant_color: '#FFD900',
            image_image_dominant_color: '#FFD900',
          },
        },
      ],
      deeplinkings: [
        {
          app: {
            id: 197,
            name: 'Rakuten TV',
            code: 'rakuten_tv',
            icon: 'https://d998oz1ebgwp7.cloudfront.net:443/img/m7l2xyokayosn3d4j2e93ts5a3fw',
            type: 'app',
            classification: {
              id: 9,
              name: 'APTA',
              description: 'Universal: suitable for all',
            },
            image_dominant_color: '#000000',
            url: 'https://prod-philips-ui40-app.rakuten.tv/',
            app_type: 'NetTV',
            resolution: '1080p',
            content_list_app_type: 'rakuten_tv_list',
            content_list_app_name: 'Últimas películas de Rakuten TV',
            description:
              'Rakuten TV es una de las principales plataformas de vídeo bajo demanda en Europa',
            background_image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/4e82tsmi4azt096ofg6r03crnzjd',
            logo: null,
            icon_landscape:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/8jd28avlz42odrxmfm5f0xvxm2e4',
            background_image_dominant_color: null,
            icon_landscape_dominant_color: null,
            icon_dominant_color: '#000000',
          },
          url: 'https://prod-philips-ui40-app.rakuten.tv/?titleid=$movies/the-lovers',
        },
      ],
    },
    {
      id: 134,
      type: 'movie',
      original_title: 'Dear Eleanor',
      title: 'Querida Eleanor',
      synopsis:
        'Max (Isabelle Fuhrman) y Ellie (Liana Liberato), dos amigas inseparables, deciden abandonar su hogar en un pueblo rural y cruzar el país en coche para conocer a la ex primera dama de los Estados Unidos, Eleanor Roosevelt. Con la radio a tope y las melenas al viento, las dos amigas se escapan en el descapotable azul del padre de Ellie (Luke Wilson), alguien que, junto a la policía, procuran eludir durante su viaje. Por el camino se cruzan con toda clase de personas que hacen de esta una aventura fascinante y única en sus vidas. El reparto también incluye a Jessica Alba, Josh Lucas, Ione Skye, Joel Courtney y Patrick Schwarzenegger.',
      year: 2016,
      classification: {
        id: 9,
        name: 'APTA',
        description: 'Universal: suitable for all',
      },
      duration_in_seconds: null,
      number: null,
      parent_id: null,
      images: {
        id: 73100,
        locale: 'es',
        artwork_portrait:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/idyt9y1fkz1cz79zb4wh1bnyupgc',
        artwork_landscape: null,
        screenshot_portrait: null,
        screenshot_landscape:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/fzimpzdpu3u96fytrmyzw2n1ccim',
        image_dominant_color: '#000000',
        transparent_logo: null,
        artwork_portrait_dominant_color: null,
        artwork_landscape_dominant_color: null,
        screenshot_portrait_dominant_color: null,
        screenshot_landscape_dominant_color: '#FF0400',
      },
      stream_info: {},
      genres: [
        {
          id: 2,
          name: 'Aventuras',
          image: {
            id: 2,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/6dpkld6r6i96kaaac34kj1opkjde',
            image_dominant_color: '#00FFF7',
            image_image_dominant_color: '#00FFF7',
          },
        },
        {
          id: 8,
          name: 'Comedia',
          image: {
            id: 8,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/13ypbzbnsgayd85gnuo72g5el7l0',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 11,
          name: 'Drama',
          image: {
            id: 11,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/htkmlixr0gd1ogf5t4aid2mazhey',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 14,
          name: 'Indie',
          image: {
            id: 14,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/nbv025yrsssr42n26oxxpkv1qlgu',
            image_dominant_color: '#00A1FF',
            image_image_dominant_color: '#00A1FF',
          },
        },
      ],
      deeplinkings: [
        {
          app: {
            id: 197,
            name: 'Rakuten TV',
            code: 'rakuten_tv',
            icon: 'https://d998oz1ebgwp7.cloudfront.net:443/img/m7l2xyokayosn3d4j2e93ts5a3fw',
            type: 'app',
            classification: {
              id: 9,
              name: 'APTA',
              description: 'Universal: suitable for all',
            },
            image_dominant_color: '#000000',
            url: 'https://prod-philips-ui40-app.rakuten.tv/',
            app_type: 'NetTV',
            resolution: '1080p',
            content_list_app_type: 'rakuten_tv_list',
            content_list_app_name: 'Últimas películas de Rakuten TV',
            description:
              'Rakuten TV es una de las principales plataformas de vídeo bajo demanda en Europa',
            background_image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/4e82tsmi4azt096ofg6r03crnzjd',
            logo: null,
            icon_landscape:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/8jd28avlz42odrxmfm5f0xvxm2e4',
            background_image_dominant_color: null,
            icon_landscape_dominant_color: null,
            icon_dominant_color: '#000000',
          },
          url: 'https://prod-philips-ui40-app.rakuten.tv/?titleid=$movies/querida-eleanor',
        },
      ],
    },
    {
      id: 796,
      type: 'movie',
      original_title: 'Feed',
      title: 'Eternamente Hermanos',
      synopsis:
        'Olivia y Matthew Grey, gemelos de 18 años de edad, han nacido en un mundo de privilegios y altas expectativas. Casi no hay límites entre ellos, incluso sus sueños están conectados. Los gemelos se preparan para su último año de instituto juntos, pero una tragedia inesperada los separa...',
      year: 2017,
      classification: {
        id: 12,
        name: '13',
        description: 'Not recommended for children under 13 years',
      },
      duration_in_seconds: null,
      number: null,
      parent_id: null,
      images: {
        id: 72768,
        locale: 'es',
        artwork_portrait:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/cx7g125llmvrxvdcp33xl8ycdd47',
        artwork_landscape: null,
        screenshot_portrait: null,
        screenshot_landscape:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/zx48wd2jbdg9abewcba3xmtowdov',
        image_dominant_color: '#000000',
        transparent_logo: null,
        artwork_portrait_dominant_color: '#FF0073',
        artwork_landscape_dominant_color: null,
        screenshot_portrait_dominant_color: null,
        screenshot_landscape_dominant_color: null,
      },
      stream_info: {},
      genres: [
        {
          id: 11,
          name: 'Drama',
          image: {
            id: 11,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/htkmlixr0gd1ogf5t4aid2mazhey',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 14,
          name: 'Indie',
          image: {
            id: 14,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/nbv025yrsssr42n26oxxpkv1qlgu',
            image_dominant_color: '#00A1FF',
            image_image_dominant_color: '#00A1FF',
          },
        },
      ],
      deeplinkings: [
        {
          app: {
            id: 197,
            name: 'Rakuten TV',
            code: 'rakuten_tv',
            icon: 'https://d998oz1ebgwp7.cloudfront.net:443/img/m7l2xyokayosn3d4j2e93ts5a3fw',
            type: 'app',
            classification: {
              id: 9,
              name: 'APTA',
              description: 'Universal: suitable for all',
            },
            image_dominant_color: '#000000',
            url: 'https://prod-philips-ui40-app.rakuten.tv/',
            app_type: 'NetTV',
            resolution: '1080p',
            content_list_app_type: 'rakuten_tv_list',
            content_list_app_name: 'Últimas películas de Rakuten TV',
            description:
              'Rakuten TV es una de las principales plataformas de vídeo bajo demanda en Europa',
            background_image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/4e82tsmi4azt096ofg6r03crnzjd',
            logo: null,
            icon_landscape:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/8jd28avlz42odrxmfm5f0xvxm2e4',
            background_image_dominant_color: null,
            icon_landscape_dominant_color: null,
            icon_dominant_color: '#000000',
          },
          url: 'https://prod-philips-ui40-app.rakuten.tv/?titleid=$movies/eternamente-hermanos',
        },
      ],
    },
    {
      id: 861,
      type: 'movie',
      original_title: 'Certain Women',
      title: 'Vidas de mujer',
      synopsis:
        'Multipremiada cinta independiente aclamada por la crítica que supone un peculiar y fascinante retrato cuidadosamente elaborado sobre la vida de tres mujeres que se cruzan en un pequeño pueblo de Estados Unidos, donde cada una está intentando abrirse camino sin demasiado éxito.',
      year: 2016,
      classification: {
        id: 14,
        name: '18',
        description: 'Not recommended for children under 18 years',
      },
      duration_in_seconds: null,
      number: null,
      parent_id: null,
      images: {
        id: 72707,
        locale: 'es',
        artwork_portrait:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/avpxnl943vhd9oh6jwqomwv2qhvx',
        artwork_landscape: null,
        screenshot_portrait: null,
        screenshot_landscape:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/qkr37gjd32c4hrxcx7afobpujffq',
        image_dominant_color: '#000000',
        transparent_logo: null,
        artwork_portrait_dominant_color: '#FF9100',
        artwork_landscape_dominant_color: null,
        screenshot_portrait_dominant_color: null,
        screenshot_landscape_dominant_color: null,
      },
      stream_info: {},
      genres: [
        {
          id: 11,
          name: 'Drama',
          image: {
            id: 11,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/htkmlixr0gd1ogf5t4aid2mazhey',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 14,
          name: 'Indie',
          image: {
            id: 14,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/nbv025yrsssr42n26oxxpkv1qlgu',
            image_dominant_color: '#00A1FF',
            image_image_dominant_color: '#00A1FF',
          },
        },
      ],
      deeplinkings: [
        {
          app: {
            id: 197,
            name: 'Rakuten TV',
            code: 'rakuten_tv',
            icon: 'https://d998oz1ebgwp7.cloudfront.net:443/img/m7l2xyokayosn3d4j2e93ts5a3fw',
            type: 'app',
            classification: {
              id: 9,
              name: 'APTA',
              description: 'Universal: suitable for all',
            },
            image_dominant_color: '#000000',
            url: 'https://prod-philips-ui40-app.rakuten.tv/',
            app_type: 'NetTV',
            resolution: '1080p',
            content_list_app_type: 'rakuten_tv_list',
            content_list_app_name: 'Últimas películas de Rakuten TV',
            description:
              'Rakuten TV es una de las principales plataformas de vídeo bajo demanda en Europa',
            background_image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/4e82tsmi4azt096ofg6r03crnzjd',
            logo: null,
            icon_landscape:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/8jd28avlz42odrxmfm5f0xvxm2e4',
            background_image_dominant_color: null,
            icon_landscape_dominant_color: null,
            icon_dominant_color: '#000000',
          },
          url: 'https://prod-philips-ui40-app.rakuten.tv/?titleid=$movies/vidas-de-mujer',
        },
      ],
    },
    {
      id: 1211,
      type: 'movie',
      original_title: 'Call Me By Your Name',
      title: 'Call me by your name',
      synopsis:
        'Timothée Chalamet interpreta a un joven de 17 años que pasa el verano en la casa de sus padres del norte de Italia. Cuando conozca al nuevo ayudante de su padre (Armie Hammer), surgirá entre ambos una fuerte atracción.',
      year: 2017,
      classification: {
        id: 11,
        name: '12',
        description: 'Not recommended for children under 12 years',
      },
      duration_in_seconds: null,
      number: null,
      parent_id: null,
      images: {
        id: 72277,
        locale: 'es',
        artwork_portrait:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/gb03syw55obuy7n7lwywu0xcauax',
        artwork_landscape: null,
        screenshot_portrait: null,
        screenshot_landscape:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/ru9s7bznhii985f0pbl8536lodvp',
        image_dominant_color: '#000000',
        transparent_logo: null,
        artwork_portrait_dominant_color: '#006AFF',
        artwork_landscape_dominant_color: null,
        screenshot_portrait_dominant_color: null,
        screenshot_landscape_dominant_color: null,
      },
      stream_info: {},
      genres: [
        {
          id: 11,
          name: 'Drama',
          image: {
            id: 11,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/htkmlixr0gd1ogf5t4aid2mazhey',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 14,
          name: 'Indie',
          image: {
            id: 14,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/nbv025yrsssr42n26oxxpkv1qlgu',
            image_dominant_color: '#00A1FF',
            image_image_dominant_color: '#00A1FF',
          },
        },
        {
          id: 19,
          name: 'Romance',
          image: {
            id: 19,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/5f6eiyrpd8cw29lh0ei9txmws5or',
            image_dominant_color: '#FFD900',
            image_image_dominant_color: '#FFD900',
          },
        },
      ],
      deeplinkings: [
        {
          app: {
            id: 197,
            name: 'Rakuten TV',
            code: 'rakuten_tv',
            icon: 'https://d998oz1ebgwp7.cloudfront.net:443/img/m7l2xyokayosn3d4j2e93ts5a3fw',
            type: 'app',
            classification: {
              id: 9,
              name: 'APTA',
              description: 'Universal: suitable for all',
            },
            image_dominant_color: '#000000',
            url: 'https://prod-philips-ui40-app.rakuten.tv/',
            app_type: 'NetTV',
            resolution: '1080p',
            content_list_app_type: 'rakuten_tv_list',
            content_list_app_name: 'Últimas películas de Rakuten TV',
            description:
              'Rakuten TV es una de las principales plataformas de vídeo bajo demanda en Europa',
            background_image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/4e82tsmi4azt096ofg6r03crnzjd',
            logo: null,
            icon_landscape:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/8jd28avlz42odrxmfm5f0xvxm2e4',
            background_image_dominant_color: null,
            icon_landscape_dominant_color: null,
            icon_dominant_color: '#000000',
          },
          url: 'https://prod-philips-ui40-app.rakuten.tv/?titleid=$movies/call-me-by-your-name',
        },
      ],
    },
    {
      id: 1560,
      type: 'movie',
      original_title: "Don't Breathe",
      title: 'No respires (Unos ojos en la oscuridad)',
      synopsis:
        "Las mentes retorcidas que crearon 'Posesión infernal' presentan este angustioso y hábil thriller lleno de tensión que se ha convertido en el título inde USA de terror del año. Tres jóvenes ladrones (Jane Levy, Dylan Minnette, Daniel Zovatto) luchan por su vida tras allanar la morada de un ciego (Stephen Lang) que esconde un sorprendente lado oscuro.",
      year: 2016,
      classification: {
        id: 13,
        name: '16',
        description: 'Not recommended for children under 16 years',
      },
      duration_in_seconds: null,
      number: null,
      parent_id: null,
      images: {
        id: 72975,
        locale: 'es',
        artwork_portrait:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/1s4q5lnlp35o163ehrip1ojkws2p',
        artwork_landscape: null,
        screenshot_portrait: null,
        screenshot_landscape:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/p1f05eut6bimy7uaq318vx6v3re6',
        image_dominant_color: '#000000',
        transparent_logo: null,
        artwork_portrait_dominant_color: null,
        artwork_landscape_dominant_color: null,
        screenshot_portrait_dominant_color: null,
        screenshot_landscape_dominant_color: '#00C3FF',
      },
      stream_info: {},
      genres: [
        {
          id: 13,
          name: 'Terror',
          image: {
            id: 13,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/2tiocei74d9nt2trpfjhauvff5dr',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 14,
          name: 'Indie',
          image: {
            id: 14,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/nbv025yrsssr42n26oxxpkv1qlgu',
            image_dominant_color: '#00A1FF',
            image_image_dominant_color: '#00A1FF',
          },
        },
        {
          id: 17,
          name: 'Suspense',
          image: {
            id: 17,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/rtb9802stczd3ym8jyan0ryrswld',
            image_dominant_color: '#FF8C00',
            image_image_dominant_color: '#FF8C00',
          },
        },
        {
          id: 21,
          name: 'Thriller',
          image: {
            id: 21,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/gsgymckl8saldslo6z1o24dqald5',
            image_dominant_color: '#0095FF',
            image_image_dominant_color: '#0095FF',
          },
        },
      ],
      deeplinkings: [
        {
          app: {
            id: 197,
            name: 'Rakuten TV',
            code: 'rakuten_tv',
            icon: 'https://d998oz1ebgwp7.cloudfront.net:443/img/m7l2xyokayosn3d4j2e93ts5a3fw',
            type: 'app',
            classification: {
              id: 9,
              name: 'APTA',
              description: 'Universal: suitable for all',
            },
            image_dominant_color: '#000000',
            url: 'https://prod-philips-ui40-app.rakuten.tv/',
            app_type: 'NetTV',
            resolution: '1080p',
            content_list_app_type: 'rakuten_tv_list',
            content_list_app_name: 'Últimas películas de Rakuten TV',
            description:
              'Rakuten TV es una de las principales plataformas de vídeo bajo demanda en Europa',
            background_image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/4e82tsmi4azt096ofg6r03crnzjd',
            logo: null,
            icon_landscape:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/8jd28avlz42odrxmfm5f0xvxm2e4',
            background_image_dominant_color: null,
            icon_landscape_dominant_color: null,
            icon_dominant_color: '#000000',
          },
          url: 'https://prod-philips-ui40-app.rakuten.tv/?titleid=$movies/no-respires-unos-ojos-en-la-oscuridad',
        },
      ],
    },
    {
      id: 1616,
      type: 'movie',
      original_title: 'Song One',
      title: 'La vida en una canción',
      synopsis:
        'Música y romance en esta película de sensible banda sonora que narra la historia de una mujer (la estrella de Hollywood, Anne Hathaway) que, tras conocer que su hermano está gravemente herido, regresa a casa para comenzar por casualidad una relación con el músico favorito de su hermano.',
      year: 2014,
      classification: {
        id: 13,
        name: '16',
        description: 'Not recommended for children under 16 years',
      },
      duration_in_seconds: null,
      number: null,
      parent_id: null,
      images: {
        id: 73528,
        locale: 'es',
        artwork_portrait:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/zgsccw7zpu4tv1dmb310gf7k2a60',
        artwork_landscape: null,
        screenshot_portrait: null,
        screenshot_landscape:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/idfpr7zgvxvpcc569ueny8ud5udt',
        image_dominant_color: '#000000',
        transparent_logo: null,
        artwork_portrait_dominant_color: '#0044FF',
        artwork_landscape_dominant_color: null,
        screenshot_portrait_dominant_color: null,
        screenshot_landscape_dominant_color: null,
      },
      stream_info: {},
      genres: [
        {
          id: 11,
          name: 'Drama',
          image: {
            id: 11,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/htkmlixr0gd1ogf5t4aid2mazhey',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 14,
          name: 'Indie',
          image: {
            id: 14,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/nbv025yrsssr42n26oxxpkv1qlgu',
            image_dominant_color: '#00A1FF',
            image_image_dominant_color: '#00A1FF',
          },
        },
        {
          id: 19,
          name: 'Romance',
          image: {
            id: 19,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/5f6eiyrpd8cw29lh0ei9txmws5or',
            image_dominant_color: '#FFD900',
            image_image_dominant_color: '#FFD900',
          },
        },
      ],
      deeplinkings: [
        {
          app: {
            id: 197,
            name: 'Rakuten TV',
            code: 'rakuten_tv',
            icon: 'https://d998oz1ebgwp7.cloudfront.net:443/img/m7l2xyokayosn3d4j2e93ts5a3fw',
            type: 'app',
            classification: {
              id: 9,
              name: 'APTA',
              description: 'Universal: suitable for all',
            },
            image_dominant_color: '#000000',
            url: 'https://prod-philips-ui40-app.rakuten.tv/',
            app_type: 'NetTV',
            resolution: '1080p',
            content_list_app_type: 'rakuten_tv_list',
            content_list_app_name: 'Últimas películas de Rakuten TV',
            description:
              'Rakuten TV es una de las principales plataformas de vídeo bajo demanda en Europa',
            background_image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/4e82tsmi4azt096ofg6r03crnzjd',
            logo: null,
            icon_landscape:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/8jd28avlz42odrxmfm5f0xvxm2e4',
            background_image_dominant_color: null,
            icon_landscape_dominant_color: null,
            icon_dominant_color: '#000000',
          },
          url: 'https://prod-philips-ui40-app.rakuten.tv/?titleid=$movies/la-vida-en-una-cancion',
        },
      ],
    },
    {
      id: 1717,
      type: 'movie',
      original_title: 'Friends with money',
      title: 'Amigos con dinero',
      synopsis:
        'Una divertida mirada a los inesperados desafíos que surgen cuando maduras. La película muestra la realidad a la que te enfrentas cuando buscas tu lugar en el mundo, habla sobre la honestidad hacia uno mismo y hacia la gente que quieres; sobre la manera de enfrentarte a tu sexualidad y atractivo. Porque aunque ya han pasado etapas importantes de la vida, aún quedan muchas más que hay que aprovechar y vivir. Tras examinar sus relaciones amorosas y amistosas, cuatro amigas descubren que no les gusta la vida que llevan y deciden cambiarla.',
      year: 2006,
      classification: {
        id: 12,
        name: '13',
        description: 'Not recommended for children under 13 years',
      },
      duration_in_seconds: null,
      number: null,
      parent_id: null,
      images: {
        id: 74881,
        locale: 'es',
        artwork_portrait:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/qqwf3naypffmjrjgjswv97xqci4c',
        artwork_landscape: null,
        screenshot_portrait: null,
        screenshot_landscape:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/ofipt8yh0zfbcysafqzstwswjfb7',
        image_dominant_color: '#000000',
        transparent_logo: null,
        artwork_portrait_dominant_color: null,
        artwork_landscape_dominant_color: null,
        screenshot_portrait_dominant_color: null,
        screenshot_landscape_dominant_color: '#FF6200',
      },
      stream_info: {},
      genres: [
        {
          id: 8,
          name: 'Comedia',
          image: {
            id: 8,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/13ypbzbnsgayd85gnuo72g5el7l0',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 11,
          name: 'Drama',
          image: {
            id: 11,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/htkmlixr0gd1ogf5t4aid2mazhey',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 14,
          name: 'Indie',
          image: {
            id: 14,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/nbv025yrsssr42n26oxxpkv1qlgu',
            image_dominant_color: '#00A1FF',
            image_image_dominant_color: '#00A1FF',
          },
        },
        {
          id: 19,
          name: 'Romance',
          image: {
            id: 19,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/5f6eiyrpd8cw29lh0ei9txmws5or',
            image_dominant_color: '#FFD900',
            image_image_dominant_color: '#FFD900',
          },
        },
      ],
      deeplinkings: [
        {
          app: {
            id: 197,
            name: 'Rakuten TV',
            code: 'rakuten_tv',
            icon: 'https://d998oz1ebgwp7.cloudfront.net:443/img/m7l2xyokayosn3d4j2e93ts5a3fw',
            type: 'app',
            classification: {
              id: 9,
              name: 'APTA',
              description: 'Universal: suitable for all',
            },
            image_dominant_color: '#000000',
            url: 'https://prod-philips-ui40-app.rakuten.tv/',
            app_type: 'NetTV',
            resolution: '1080p',
            content_list_app_type: 'rakuten_tv_list',
            content_list_app_name: 'Últimas películas de Rakuten TV',
            description:
              'Rakuten TV es una de las principales plataformas de vídeo bajo demanda en Europa',
            background_image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/4e82tsmi4azt096ofg6r03crnzjd',
            logo: null,
            icon_landscape:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/8jd28avlz42odrxmfm5f0xvxm2e4',
            background_image_dominant_color: null,
            icon_landscape_dominant_color: null,
            icon_dominant_color: '#000000',
          },
          url: 'https://prod-philips-ui40-app.rakuten.tv/?titleid=$movies/amigos-con-dinero',
        },
      ],
    },
    {
      id: 1726,
      type: 'movie',
      original_title: 'Marie Antoinette',
      title: 'María Antonieta',
      synopsis:
        'Sofia Coppola nos ofrece una mirada única a la fascinante figura de María Antonieta con una realización impecable. Francia, siglo XVIII. El compromiso matrimonial entre el futuro Luis XVI y María Antonieta (Kirsten Dunst) sirve para sellar una alianza entre Francia y Austria. Con sólo catorce años, la ingenua princesa austríaca se ve obligada a abandonar Viena, su familia y sus amigos para instalarse en la opulenta, sofisticada y libertina corte francesa, donde reinan las intrigas y los escándalos. La joven se rebela contra el aislamiento que representa la corte de Versalles y se convierte en la reina más incomprendida de Francia.',
      year: 2006,
      classification: {
        id: 9,
        name: 'APTA',
        description: 'Universal: suitable for all',
      },
      duration_in_seconds: null,
      number: null,
      parent_id: null,
      images: {
        id: 74985,
        locale: 'es',
        artwork_portrait:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/svr87s41txy1ansl518z56d36zrg',
        artwork_landscape: null,
        screenshot_portrait: null,
        screenshot_landscape:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/1jd69a92vsxewyrqncxbveg875zk',
        image_dominant_color: '#000000',
        transparent_logo: null,
        artwork_portrait_dominant_color: '#FF4000',
        artwork_landscape_dominant_color: null,
        screenshot_portrait_dominant_color: null,
        screenshot_landscape_dominant_color: null,
      },
      stream_info: {},
      genres: [
        {
          id: 11,
          name: 'Drama',
          image: {
            id: 11,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/htkmlixr0gd1ogf5t4aid2mazhey',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 14,
          name: 'Indie',
          image: {
            id: 14,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/nbv025yrsssr42n26oxxpkv1qlgu',
            image_dominant_color: '#00A1FF',
            image_image_dominant_color: '#00A1FF',
          },
        },
      ],
      deeplinkings: [
        {
          app: {
            id: 197,
            name: 'Rakuten TV',
            code: 'rakuten_tv',
            icon: 'https://d998oz1ebgwp7.cloudfront.net:443/img/m7l2xyokayosn3d4j2e93ts5a3fw',
            type: 'app',
            classification: {
              id: 9,
              name: 'APTA',
              description: 'Universal: suitable for all',
            },
            image_dominant_color: '#000000',
            url: 'https://prod-philips-ui40-app.rakuten.tv/',
            app_type: 'NetTV',
            resolution: '1080p',
            content_list_app_type: 'rakuten_tv_list',
            content_list_app_name: 'Últimas películas de Rakuten TV',
            description:
              'Rakuten TV es una de las principales plataformas de vídeo bajo demanda en Europa',
            background_image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/4e82tsmi4azt096ofg6r03crnzjd',
            logo: null,
            icon_landscape:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/8jd28avlz42odrxmfm5f0xvxm2e4',
            background_image_dominant_color: null,
            icon_landscape_dominant_color: null,
            icon_dominant_color: '#000000',
          },
          url: 'https://prod-philips-ui40-app.rakuten.tv/?titleid=$movies/maria-antonieta',
        },
      ],
    },
    {
      id: 1891,
      type: 'movie',
      original_title: 'Sleight',
      title: 'Juegos de manos',
      synopsis:
        "'Juego de manos' es imaginativa y refrescante según va transformándose sin esfuerzo a través de sus giros narrativos e inventando, al mismo tiempo, algo único e inesperado. Un joven mago callejero encargado de cuidar a su hermana pequeña tras la muerte de su madre se convierte en un pequeño traficante de droga de la escena de fiesta de Los Ángeles para mantener un techo bajo el que vivir. Cuando se mete en problemas con su proveedor, su hermana es raptada y deberá apoyarse en su mente brillante y sus juegos de manos para salvarla.",
      year: 2016,
      classification: {
        id: 12,
        name: '13',
        description: 'Not recommended for children under 13 years',
      },
      duration_in_seconds: null,
      number: null,
      parent_id: null,
      images: {
        id: 72504,
        locale: 'es',
        artwork_portrait:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/fa7qiszaxbxusg0qf4ipmoc2idr1',
        artwork_landscape: null,
        screenshot_portrait: null,
        screenshot_landscape:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/pmnttrmfr6f7tqsbd3gchychtf4g',
        image_dominant_color: '#000000',
        transparent_logo: null,
        artwork_portrait_dominant_color: '#FF0022',
        artwork_landscape_dominant_color: null,
        screenshot_portrait_dominant_color: null,
        screenshot_landscape_dominant_color: null,
      },
      stream_info: {},
      genres: [
        {
          id: 1,
          name: 'Acción',
          image: {
            id: 1,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/k6uv0nzz7rjq40qsfygg7sj9wbo0',
            image_dominant_color: '#FF6A00',
            image_image_dominant_color: '#FF6A00',
          },
        },
        {
          id: 11,
          name: 'Drama',
          image: {
            id: 11,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/htkmlixr0gd1ogf5t4aid2mazhey',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 12,
          name: 'Fantasía',
          image: {
            id: 12,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/ddblwtg7d8qorumamw7qhqcj8fw5',
            image_dominant_color: '#008CFF',
            image_image_dominant_color: '#008CFF',
          },
        },
        {
          id: 14,
          name: 'Indie',
          image: {
            id: 14,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/nbv025yrsssr42n26oxxpkv1qlgu',
            image_dominant_color: '#00A1FF',
            image_image_dominant_color: '#00A1FF',
          },
        },
        {
          id: 20,
          name: 'Ciencia ficción',
          image: {
            id: 20,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/wcz0kjhw62bpa789ob5as8kwyi6p',
            image_dominant_color: '#00BBFF',
            image_image_dominant_color: '#00BBFF',
          },
        },
        {
          id: 21,
          name: 'Thriller',
          image: {
            id: 21,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/gsgymckl8saldslo6z1o24dqald5',
            image_dominant_color: '#0095FF',
            image_image_dominant_color: '#0095FF',
          },
        },
      ],
      deeplinkings: [
        {
          app: {
            id: 197,
            name: 'Rakuten TV',
            code: 'rakuten_tv',
            icon: 'https://d998oz1ebgwp7.cloudfront.net:443/img/m7l2xyokayosn3d4j2e93ts5a3fw',
            type: 'app',
            classification: {
              id: 9,
              name: 'APTA',
              description: 'Universal: suitable for all',
            },
            image_dominant_color: '#000000',
            url: 'https://prod-philips-ui40-app.rakuten.tv/',
            app_type: 'NetTV',
            resolution: '1080p',
            content_list_app_type: 'rakuten_tv_list',
            content_list_app_name: 'Últimas películas de Rakuten TV',
            description:
              'Rakuten TV es una de las principales plataformas de vídeo bajo demanda en Europa',
            background_image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/4e82tsmi4azt096ofg6r03crnzjd',
            logo: null,
            icon_landscape:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/8jd28avlz42odrxmfm5f0xvxm2e4',
            background_image_dominant_color: null,
            icon_landscape_dominant_color: null,
            icon_dominant_color: '#000000',
          },
          url: 'https://prod-philips-ui40-app.rakuten.tv/?titleid=$movies/juegos-de-manos',
        },
      ],
    },
    {
      id: 1908,
      type: 'movie',
      original_title: 'The Bronze',
      title: 'Bronce',
      synopsis:
        'En 2004 Hope Ann Greggory se convirtió en una heroí­na americana tras ganar la medalla de bronce. Ahora, atrapada en su gloria pasada, vive en el pequeño sótano de su padre. Pero la llegada de una nueva atleta en la ciudad obliga a Hope a luchar por defender su estatus de celebridad local. La ex-estrella tendrá que hacer frente a una complica decisión: ayudar a la joven que la idolatra o intentar acabar con su éxito.',
      year: 2016,
      classification: {
        id: 14,
        name: '18',
        description: 'Not recommended for children under 18 years',
      },
      duration_in_seconds: null,
      number: null,
      parent_id: null,
      images: {
        id: 73511,
        locale: 'es',
        artwork_portrait:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/a8s7w6o8fri118n98zed8cq5khsg',
        artwork_landscape: null,
        screenshot_portrait: null,
        screenshot_landscape:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/j8fvppokzn07c0ufou0dc38ode67',
        image_dominant_color: '#000000',
        transparent_logo: null,
        artwork_portrait_dominant_color: null,
        artwork_landscape_dominant_color: null,
        screenshot_portrait_dominant_color: null,
        screenshot_landscape_dominant_color: '#FF0004',
      },
      stream_info: {},
      genres: [
        {
          id: 8,
          name: 'Comedia',
          image: {
            id: 8,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/13ypbzbnsgayd85gnuo72g5el7l0',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 11,
          name: 'Drama',
          image: {
            id: 11,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/htkmlixr0gd1ogf5t4aid2mazhey',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 14,
          name: 'Indie',
          image: {
            id: 14,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/nbv025yrsssr42n26oxxpkv1qlgu',
            image_dominant_color: '#00A1FF',
            image_image_dominant_color: '#00A1FF',
          },
        },
      ],
      deeplinkings: [
        {
          app: {
            id: 197,
            name: 'Rakuten TV',
            code: 'rakuten_tv',
            icon: 'https://d998oz1ebgwp7.cloudfront.net:443/img/m7l2xyokayosn3d4j2e93ts5a3fw',
            type: 'app',
            classification: {
              id: 9,
              name: 'APTA',
              description: 'Universal: suitable for all',
            },
            image_dominant_color: '#000000',
            url: 'https://prod-philips-ui40-app.rakuten.tv/',
            app_type: 'NetTV',
            resolution: '1080p',
            content_list_app_type: 'rakuten_tv_list',
            content_list_app_name: 'Últimas películas de Rakuten TV',
            description:
              'Rakuten TV es una de las principales plataformas de vídeo bajo demanda en Europa',
            background_image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/4e82tsmi4azt096ofg6r03crnzjd',
            logo: null,
            icon_landscape:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/8jd28avlz42odrxmfm5f0xvxm2e4',
            background_image_dominant_color: null,
            icon_landscape_dominant_color: null,
            icon_dominant_color: '#000000',
          },
          url: 'https://prod-philips-ui40-app.rakuten.tv/?titleid=$movies/bronce',
        },
      ],
    },
    {
      id: 1969,
      type: 'movie',
      original_title: 'Two Lovers and a Bear',
      title: 'Dos amantes y un oso',
      synopsis:
        'Roman y Lucy son dos personas que viven en una pequeña y helada ciudad del Archipiélago Ártico Canadiense. Él huye de su pasado, mientras que ella está más preocupada por lo que ofrece el presente y anhela regresar a la civilización para volver a la universidad. En ese inhóspito lugar, duele más su bagaje emocional que el frío que congela sus huesos.',
      year: 2016,
      classification: {
        id: 13,
        name: '16',
        description: 'Not recommended for children under 16 years',
      },
      duration_in_seconds: null,
      number: null,
      parent_id: null,
      images: {
        id: 72642,
        locale: 'es',
        artwork_portrait:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/sb7uzfhhrw9yn8sorivs2ucuv0j5',
        artwork_landscape: null,
        screenshot_portrait: null,
        screenshot_landscape:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/2nta5unbzl1vxant626tlf72unhu',
        image_dominant_color: '#000000',
        transparent_logo: null,
        artwork_portrait_dominant_color: '#0066FF',
        artwork_landscape_dominant_color: null,
        screenshot_portrait_dominant_color: null,
        screenshot_landscape_dominant_color: null,
      },
      stream_info: {},
      genres: [
        {
          id: 2,
          name: 'Aventuras',
          image: {
            id: 2,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/6dpkld6r6i96kaaac34kj1opkjde',
            image_dominant_color: '#00FFF7',
            image_image_dominant_color: '#00FFF7',
          },
        },
        {
          id: 11,
          name: 'Drama',
          image: {
            id: 11,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/htkmlixr0gd1ogf5t4aid2mazhey',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 14,
          name: 'Indie',
          image: {
            id: 14,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/nbv025yrsssr42n26oxxpkv1qlgu',
            image_dominant_color: '#00A1FF',
            image_image_dominant_color: '#00A1FF',
          },
        },
        {
          id: 19,
          name: 'Romance',
          image: {
            id: 19,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/5f6eiyrpd8cw29lh0ei9txmws5or',
            image_dominant_color: '#FFD900',
            image_image_dominant_color: '#FFD900',
          },
        },
      ],
      deeplinkings: [
        {
          app: {
            id: 197,
            name: 'Rakuten TV',
            code: 'rakuten_tv',
            icon: 'https://d998oz1ebgwp7.cloudfront.net:443/img/m7l2xyokayosn3d4j2e93ts5a3fw',
            type: 'app',
            classification: {
              id: 9,
              name: 'APTA',
              description: 'Universal: suitable for all',
            },
            image_dominant_color: '#000000',
            url: 'https://prod-philips-ui40-app.rakuten.tv/',
            app_type: 'NetTV',
            resolution: '1080p',
            content_list_app_type: 'rakuten_tv_list',
            content_list_app_name: 'Últimas películas de Rakuten TV',
            description:
              'Rakuten TV es una de las principales plataformas de vídeo bajo demanda en Europa',
            background_image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/4e82tsmi4azt096ofg6r03crnzjd',
            logo: null,
            icon_landscape:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/8jd28avlz42odrxmfm5f0xvxm2e4',
            background_image_dominant_color: null,
            icon_landscape_dominant_color: null,
            icon_dominant_color: '#000000',
          },
          url: 'https://prod-philips-ui40-app.rakuten.tv/?titleid=$movies/dos-amantes-y-un-oso',
        },
      ],
    },
    {
      id: 2049,
      type: 'movie',
      original_title: 'Grandma',
      title: 'Grandma',
      synopsis:
        'Elle Reid (Lily Tomlin) está asimilando la ruptura con su novia cuando su nieta, Sage, aparece inesperadamente. Necesita 600 dólares antes de que llegue la noche. Temporalmente arruinadas, la abuela Elle y Sage se pasan el día intentando conseguir el dinero mientras sus visitas a viejos amigos y amantes desentierran recuerdos y secretos olvidados.',
      year: 2015,
      classification: {
        id: 11,
        name: '12',
        description: 'Not recommended for children under 12 years',
      },
      duration_in_seconds: null,
      number: null,
      parent_id: null,
      images: {
        id: 73414,
        locale: 'es',
        artwork_portrait:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/u52193f45n2229a44un4blhmgms4',
        artwork_landscape: null,
        screenshot_portrait: null,
        screenshot_landscape:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/15ju5ksh6arimwdrfpxl8xq2rkl9',
        image_dominant_color: '#000000',
        transparent_logo: null,
        artwork_portrait_dominant_color: null,
        artwork_landscape_dominant_color: null,
        screenshot_portrait_dominant_color: null,
        screenshot_landscape_dominant_color: '#00E5FF',
      },
      stream_info: {},
      genres: [
        {
          id: 8,
          name: 'Comedia',
          image: {
            id: 8,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/13ypbzbnsgayd85gnuo72g5el7l0',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 11,
          name: 'Drama',
          image: {
            id: 11,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/htkmlixr0gd1ogf5t4aid2mazhey',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 14,
          name: 'Indie',
          image: {
            id: 14,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/nbv025yrsssr42n26oxxpkv1qlgu',
            image_dominant_color: '#00A1FF',
            image_image_dominant_color: '#00A1FF',
          },
        },
      ],
      deeplinkings: [
        {
          app: {
            id: 197,
            name: 'Rakuten TV',
            code: 'rakuten_tv',
            icon: 'https://d998oz1ebgwp7.cloudfront.net:443/img/m7l2xyokayosn3d4j2e93ts5a3fw',
            type: 'app',
            classification: {
              id: 9,
              name: 'APTA',
              description: 'Universal: suitable for all',
            },
            image_dominant_color: '#000000',
            url: 'https://prod-philips-ui40-app.rakuten.tv/',
            app_type: 'NetTV',
            resolution: '1080p',
            content_list_app_type: 'rakuten_tv_list',
            content_list_app_name: 'Últimas películas de Rakuten TV',
            description:
              'Rakuten TV es una de las principales plataformas de vídeo bajo demanda en Europa',
            background_image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/4e82tsmi4azt096ofg6r03crnzjd',
            logo: null,
            icon_landscape:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/8jd28avlz42odrxmfm5f0xvxm2e4',
            background_image_dominant_color: null,
            icon_landscape_dominant_color: null,
            icon_dominant_color: '#000000',
          },
          url: 'https://prod-philips-ui40-app.rakuten.tv/?titleid=$movies/grandma',
        },
      ],
    },
    {
      id: 2238,
      type: 'movie',
      original_title:
        'Dr. Strangelove or: How I Learned to Stop Worrying and Love the Bomb',
      title: '¿Teléfono rojo? Volamos hacia Moscú',
      synopsis:
        'Demoledora sátira sobre la guerra Fría y las teorías paranoicas de conspiración entre los Estados Unidos y la Unión Soviética. El maestro Stanley Kubrick dirige esta delirante comedia ya convertida en clásico con Peter Sellers, brillante como siempre. Convencido de que los comunistas están contaminando a la nación americana, un general ordena, en un acceso de locura, un ataque aéreo nuclear por sorpresa sobre la Unión Soviética. Su ayudante, el capitán Mandrake trata de averiguar el código para detener el bombardeo.',
      year: 1964,
      classification: {
        id: 14,
        name: '18',
        description: 'Not recommended for children under 18 years',
      },
      duration_in_seconds: null,
      number: null,
      parent_id: null,
      images: {
        id: 76021,
        locale: 'es',
        artwork_portrait:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/txuyhzu9myg3b2c0q5ilrqyqzclb',
        artwork_landscape: null,
        screenshot_portrait: null,
        screenshot_landscape:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/fzvq7lmmeqcrup606yl1ocb08i3j',
        image_dominant_color: '#000000',
        transparent_logo: null,
        artwork_portrait_dominant_color: '#FF001A',
        artwork_landscape_dominant_color: null,
        screenshot_portrait_dominant_color: null,
        screenshot_landscape_dominant_color: null,
      },
      stream_info: {},
      genres: [
        {
          id: 22,
          name: 'Bélicas',
          image: {
            id: 22,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/jrurok5aqjkaleebkflwqthkiyzw',
            image_dominant_color: '#FFAA00',
            image_image_dominant_color: '#FFAA00',
          },
        },
        {
          id: 5,
          name: 'Británico',
          image: {
            id: 5,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/hnnbxpvoypz4t2dc7tyktdf9uwbp',
            image_dominant_color: '#00D9FF',
            image_image_dominant_color: '#00D9FF',
          },
        },
        {
          id: 7,
          name: 'Películas clásicas',
          image: {
            id: 7,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/ljdeli18bizxca0g4rwc018jp1pj',
            image_dominant_color: '#FFBF00',
            image_image_dominant_color: '#FFBF00',
          },
        },
        {
          id: 8,
          name: 'Comedia',
          image: {
            id: 8,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/13ypbzbnsgayd85gnuo72g5el7l0',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 14,
          name: 'Indie',
          image: {
            id: 14,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/nbv025yrsssr42n26oxxpkv1qlgu',
            image_dominant_color: '#00A1FF',
            image_image_dominant_color: '#00A1FF',
          },
        },
      ],
      deeplinkings: [
        {
          app: {
            id: 197,
            name: 'Rakuten TV',
            code: 'rakuten_tv',
            icon: 'https://d998oz1ebgwp7.cloudfront.net:443/img/m7l2xyokayosn3d4j2e93ts5a3fw',
            type: 'app',
            classification: {
              id: 9,
              name: 'APTA',
              description: 'Universal: suitable for all',
            },
            image_dominant_color: '#000000',
            url: 'https://prod-philips-ui40-app.rakuten.tv/',
            app_type: 'NetTV',
            resolution: '1080p',
            content_list_app_type: 'rakuten_tv_list',
            content_list_app_name: 'Últimas películas de Rakuten TV',
            description:
              'Rakuten TV es una de las principales plataformas de vídeo bajo demanda en Europa',
            background_image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/4e82tsmi4azt096ofg6r03crnzjd',
            logo: null,
            icon_landscape:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/8jd28avlz42odrxmfm5f0xvxm2e4',
            background_image_dominant_color: null,
            icon_landscape_dominant_color: null,
            icon_dominant_color: '#000000',
          },
          url: 'https://prod-philips-ui40-app.rakuten.tv/?titleid=$movies/telefono-rojo-volamos-hacia-moscu',
        },
      ],
    },
    {
      id: 2301,
      type: 'movie',
      original_title: 'All we had',
      title: 'Todo lo que teníamos',
      synopsis:
        'Rita (Katie Holmes) y su hija de 13 años, vivían en una humilde casa en California, sin embargo las deudas hacen que el dueño termine echándolas del lugar. Desesperada, aquella madre soltera decide mudarse a un pequeño pueblo, Fat River, donde tendrá que empezar de cero con gente nueva…',
      year: 2016,
      classification: {
        id: 11,
        name: '12',
        description: 'Not recommended for children under 12 years',
      },
      duration_in_seconds: null,
      number: null,
      parent_id: null,
      images: {
        id: 72638,
        locale: 'es',
        artwork_portrait:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/w2vaq5nxgyj9s3xarwp07kfram77',
        artwork_landscape: null,
        screenshot_portrait: null,
        screenshot_landscape:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/oa10p7t2e6zq1sfanjvi5i2f72p3',
        image_dominant_color: '#000000',
        transparent_logo: null,
        artwork_portrait_dominant_color: '#FF6600',
        artwork_landscape_dominant_color: null,
        screenshot_portrait_dominant_color: null,
        screenshot_landscape_dominant_color: null,
      },
      stream_info: {},
      genres: [
        {
          id: 11,
          name: 'Drama',
          image: {
            id: 11,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/htkmlixr0gd1ogf5t4aid2mazhey',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 14,
          name: 'Indie',
          image: {
            id: 14,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/nbv025yrsssr42n26oxxpkv1qlgu',
            image_dominant_color: '#00A1FF',
            image_image_dominant_color: '#00A1FF',
          },
        },
      ],
      deeplinkings: [
        {
          app: {
            id: 197,
            name: 'Rakuten TV',
            code: 'rakuten_tv',
            icon: 'https://d998oz1ebgwp7.cloudfront.net:443/img/m7l2xyokayosn3d4j2e93ts5a3fw',
            type: 'app',
            classification: {
              id: 9,
              name: 'APTA',
              description: 'Universal: suitable for all',
            },
            image_dominant_color: '#000000',
            url: 'https://prod-philips-ui40-app.rakuten.tv/',
            app_type: 'NetTV',
            resolution: '1080p',
            content_list_app_type: 'rakuten_tv_list',
            content_list_app_name: 'Últimas películas de Rakuten TV',
            description:
              'Rakuten TV es una de las principales plataformas de vídeo bajo demanda en Europa',
            background_image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/4e82tsmi4azt096ofg6r03crnzjd',
            logo: null,
            icon_landscape:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/8jd28avlz42odrxmfm5f0xvxm2e4',
            background_image_dominant_color: null,
            icon_landscape_dominant_color: null,
            icon_dominant_color: '#000000',
          },
          url: 'https://prod-philips-ui40-app.rakuten.tv/?titleid=$movies/todo-lo-que-teniamos',
        },
      ],
    },
    {
      id: 2306,
      type: 'movie',
      original_title: "We don't belong here",
      title: 'Nuestro sitio',
      synopsis:
        'Nancy Green es la matriarca de una familia totalmente disfuncional, formada por sus tres hijas y su hijo, Lily, Elisa, Madeline y Maxwell. La repentina desaparición de Maxwell hará que Nancy cambie radicalmente su punto de vista sobre la vida y su perspectiva haga que tenga que establecer nuevos límites en su familia. Un camino introspectivo que hará que Nancy decida dar un cambio total en su vida, teniendo que hacer frente no solamente al gran dolor de la pérdida de un hijo, sino a la preocupación de hacia dónde irán sus hijas.',
      year: 2016,
      classification: {
        id: 13,
        name: '16',
        description: 'Not recommended for children under 16 years',
      },
      duration_in_seconds: null,
      number: null,
      parent_id: null,
      images: {
        id: 72750,
        locale: 'es',
        artwork_portrait:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/sblohbopln7s23hi2llwiaws53ap',
        artwork_landscape: null,
        screenshot_portrait: null,
        screenshot_landscape:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/8gq7wv8zbfcnt6u6azaluu7orbdi',
        image_dominant_color: '#000000',
        transparent_logo: null,
        artwork_portrait_dominant_color: null,
        artwork_landscape_dominant_color: null,
        screenshot_portrait_dominant_color: null,
        screenshot_landscape_dominant_color: '#0037FF',
      },
      stream_info: {},
      genres: [
        {
          id: 11,
          name: 'Drama',
          image: {
            id: 11,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/htkmlixr0gd1ogf5t4aid2mazhey',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 14,
          name: 'Indie',
          image: {
            id: 14,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/nbv025yrsssr42n26oxxpkv1qlgu',
            image_dominant_color: '#00A1FF',
            image_image_dominant_color: '#00A1FF',
          },
        },
      ],
      deeplinkings: [
        {
          app: {
            id: 197,
            name: 'Rakuten TV',
            code: 'rakuten_tv',
            icon: 'https://d998oz1ebgwp7.cloudfront.net:443/img/m7l2xyokayosn3d4j2e93ts5a3fw',
            type: 'app',
            classification: {
              id: 9,
              name: 'APTA',
              description: 'Universal: suitable for all',
            },
            image_dominant_color: '#000000',
            url: 'https://prod-philips-ui40-app.rakuten.tv/',
            app_type: 'NetTV',
            resolution: '1080p',
            content_list_app_type: 'rakuten_tv_list',
            content_list_app_name: 'Últimas películas de Rakuten TV',
            description:
              'Rakuten TV es una de las principales plataformas de vídeo bajo demanda en Europa',
            background_image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/4e82tsmi4azt096ofg6r03crnzjd',
            logo: null,
            icon_landscape:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/8jd28avlz42odrxmfm5f0xvxm2e4',
            background_image_dominant_color: null,
            icon_landscape_dominant_color: null,
            icon_dominant_color: '#000000',
          },
          url: 'https://prod-philips-ui40-app.rakuten.tv/?titleid=$movies/nuestro-sitio',
        },
      ],
    },
    {
      id: 2319,
      type: 'movie',
      original_title: 'The Hollars',
      title: 'Los Hollars',
      synopsis:
        'Inteligente comedia dramática que narra la historia de John Hollar, un aspirante a artista que debe salir de su cómoda vida en Nueva York y alejarse de su hermosa novia para poder regresar a su ciudad natal del medio oeste y así ayudar a su familia a salir adelante cuando su madre necesita una cirugía cerebral. Esto ocurrirá mientras se divide entre su antigua vida y la actual, en la que debe lidiar con un hermano desvalido, un padre con ansiedad y su antiguo amor de la escuela.',
      year: 2016,
      classification: {
        id: 14,
        name: '18',
        description: 'Not recommended for children under 18 years',
      },
      duration_in_seconds: null,
      number: null,
      parent_id: null,
      images: {
        id: 72748,
        locale: 'es',
        artwork_portrait:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/teuxdi51ynwib7qr7yjh323cpsdd',
        artwork_landscape: null,
        screenshot_portrait: null,
        screenshot_landscape:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/x4owomkbc7mnirmayg8twgydkome',
        image_dominant_color: '#000000',
        transparent_logo: null,
        artwork_portrait_dominant_color: '#5E00FF',
        artwork_landscape_dominant_color: null,
        screenshot_portrait_dominant_color: null,
        screenshot_landscape_dominant_color: null,
      },
      stream_info: {},
      genres: [
        {
          id: 8,
          name: 'Comedia',
          image: {
            id: 8,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/13ypbzbnsgayd85gnuo72g5el7l0',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 11,
          name: 'Drama',
          image: {
            id: 11,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/htkmlixr0gd1ogf5t4aid2mazhey',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 14,
          name: 'Indie',
          image: {
            id: 14,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/nbv025yrsssr42n26oxxpkv1qlgu',
            image_dominant_color: '#00A1FF',
            image_image_dominant_color: '#00A1FF',
          },
        },
      ],
      deeplinkings: [
        {
          app: {
            id: 197,
            name: 'Rakuten TV',
            code: 'rakuten_tv',
            icon: 'https://d998oz1ebgwp7.cloudfront.net:443/img/m7l2xyokayosn3d4j2e93ts5a3fw',
            type: 'app',
            classification: {
              id: 9,
              name: 'APTA',
              description: 'Universal: suitable for all',
            },
            image_dominant_color: '#000000',
            url: 'https://prod-philips-ui40-app.rakuten.tv/',
            app_type: 'NetTV',
            resolution: '1080p',
            content_list_app_type: 'rakuten_tv_list',
            content_list_app_name: 'Últimas películas de Rakuten TV',
            description:
              'Rakuten TV es una de las principales plataformas de vídeo bajo demanda en Europa',
            background_image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/4e82tsmi4azt096ofg6r03crnzjd',
            logo: null,
            icon_landscape:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/8jd28avlz42odrxmfm5f0xvxm2e4',
            background_image_dominant_color: null,
            icon_landscape_dominant_color: null,
            icon_dominant_color: '#000000',
          },
          url: 'https://prod-philips-ui40-app.rakuten.tv/?titleid=$movies/los-hollars',
        },
      ],
    },
    {
      id: 2320,
      type: 'movie',
      original_title: 'Hello, my name is Doris',
      title: 'Hello my name is doris',
      synopsis:
        'Hello my name is Doris, es una ingeniosa comedia en la que un seminario de autoayuda inspira a la protagonista a dar una oportunidad al amor. Después de toda una vida pasando desapercibida y siendo ignorada, la vida de Doris (Sally Field), una mujer entrada en años, da un vuelco cuando se enamora de un guapo compañero de trabajo mucho más joven que ella. Su primer encuentro con el amor verdadero (fuera de las páginas de las novelas) convence a Doris de que ella y su joven compañero de trabajo están hechos el uno para el otro.',
      year: 2015,
      classification: {
        id: 9,
        name: 'APTA',
        description: 'Universal: suitable for all',
      },
      duration_in_seconds: null,
      number: null,
      parent_id: null,
      images: {
        id: 72808,
        locale: 'es',
        artwork_portrait:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/zafa61p2o0gh50dxp9l6tzs1rpmd',
        artwork_landscape: null,
        screenshot_portrait: null,
        screenshot_landscape:
          'https://d998oz1ebgwp7.cloudfront.net:443/img/8g6b2pmlufe5rnnvs7un33o1mxsc',
        image_dominant_color: '#000000',
        transparent_logo: null,
        artwork_portrait_dominant_color: null,
        artwork_landscape_dominant_color: null,
        screenshot_portrait_dominant_color: null,
        screenshot_landscape_dominant_color: '#FF3C00',
      },
      stream_info: {},
      genres: [
        {
          id: 8,
          name: 'Comedia',
          image: {
            id: 8,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/13ypbzbnsgayd85gnuo72g5el7l0',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 11,
          name: 'Drama',
          image: {
            id: 11,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/htkmlixr0gd1ogf5t4aid2mazhey',
            image_dominant_color: '#FF7700',
            image_image_dominant_color: '#FF7700',
          },
        },
        {
          id: 14,
          name: 'Indie',
          image: {
            id: 14,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/nbv025yrsssr42n26oxxpkv1qlgu',
            image_dominant_color: '#00A1FF',
            image_image_dominant_color: '#00A1FF',
          },
        },
        {
          id: 19,
          name: 'Romance',
          image: {
            id: 19,
            locale: 'en',
            image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/5f6eiyrpd8cw29lh0ei9txmws5or',
            image_dominant_color: '#FFD900',
            image_image_dominant_color: '#FFD900',
          },
        },
      ],
      deeplinkings: [
        {
          app: {
            id: 197,
            name: 'Rakuten TV',
            code: 'rakuten_tv',
            icon: 'https://d998oz1ebgwp7.cloudfront.net:443/img/m7l2xyokayosn3d4j2e93ts5a3fw',
            type: 'app',
            classification: {
              id: 9,
              name: 'APTA',
              description: 'Universal: suitable for all',
            },
            image_dominant_color: '#000000',
            url: 'https://prod-philips-ui40-app.rakuten.tv/',
            app_type: 'NetTV',
            resolution: '1080p',
            content_list_app_type: 'rakuten_tv_list',
            content_list_app_name: 'Últimas películas de Rakuten TV',
            description:
              'Rakuten TV es una de las principales plataformas de vídeo bajo demanda en Europa',
            background_image:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/4e82tsmi4azt096ofg6r03crnzjd',
            logo: null,
            icon_landscape:
              'https://d998oz1ebgwp7.cloudfront.net:443/img/8jd28avlz42odrxmfm5f0xvxm2e4',
            background_image_dominant_color: null,
            icon_landscape_dominant_color: null,
            icon_dominant_color: '#000000',
          },
          url: 'https://prod-philips-ui40-app.rakuten.tv/?titleid=$movies/hello-my-name-is-doris',
        },
      ],
    },
  ],
  pagination: {
    count: 20,
    per_page: 20,
    current_page: 1,
    next_page: 2,
    prev_page: null,
    total_count: 346,
    total_pages: 18,
  },
}

export const nextPagination: Pagination = {
  count: 20,
  per_page: 20,
  current_page: 2,
  next_page: 3,
  prev_page: 1,
  total_count: 346,
  total_pages: 18,
}
